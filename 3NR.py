import random

def dibujarTablero(tablero):
    print('   |   |')
    print(' ' + tablero[7] + ' | ' + tablero[8] + ' | ' + tablero[9])
    print('   |   |')
    print('-----------')
    print('   |   |')
    print(' ' + tablero[4] + ' | ' + tablero[5] + ' | ' + tablero[6])
    print('   |   |')
    print('-----------')
    print('   |   |')
    print(' ' + tablero[1] + ' | ' + tablero[2] + ' | ' + tablero[3])
    print('   |   |')

def introducirCarta():
    jugador = ''
    while not (jugador == 'X' or jugador == 'O'):
        print('¿Quieres ser X u O?')
        jugador = input().upper()

    if jugador == 'X':
        return ['X', 'O']
    else:
        return ['O', 'X']

def empiezaJugador():
    if random.randint(0, 1) == 0:
        return 'computadora'
    else:
        return 'jugador'

def jugarDeNuevo():
    print('¿Quieres jugar otra vez? (si or no)')
    return input().lower().startswith('s')

def hacerMovimiento(tablero, jugador, movimiento):
    tablero[movimiento] = jugador

def esGanador(bo, le):
    return ((bo[7] == le and bo[8] == le and bo[9] == le) or # across the top
    (bo[4] == le and bo[5] == le and bo[6] == le) or # across the middle
    (bo[1] == le and bo[2] == le and bo[3] == le) or # across the bottom
    (bo[7] == le and bo[4] == le and bo[1] == le) or # down the left side
    (bo[8] == le and bo[5] == le and bo[2] == le) or # down the middle
    (bo[9] == le and bo[6] == le and bo[3] == le) or # down the right side
    (bo[7] == le and bo[5] == le and bo[3] == le) or # diagonal
    (bo[9] == le and bo[5] == le and bo[1] == le)) # diagonal

def obtenerCopiaTablero(tablero):
    tableroDuplicado = []

    for i in tablero:
        tableroDuplicado.append(i)

    return tableroDuplicado

def esEspacioVacio(tablero, movimiento):
    return tablero[movimiento] == ' '

def obtenerMovimientoJugador(tablero):
    movimiento = ' '
    while movimiento not in '1 2 3 4 5 6 7 8 9'.split() or not esEspacioVacio(tablero, int(movimiento)):
        print('¿Cuál es tu próximo movimiento? (1-9)')
        movimiento = input()
    return int(movimiento)

def elegirMovimientoAleatorioLista(tablero, listaMovimientos):
    posiblesMovimientos = []
    for i in listaMovimientos:
        if esEspacioVacio(tablero, i):
            posiblesMovimientos.append(i)

    if len(posiblesMovimientos) != 0:
        return random.choice(posiblesMovimientos)
    else:
        return None

def obtenerMovimientoComputadora(tablero, cartaComputadora):
    if cartaComputadora == 'X':
        cartaJugador = 'O'
    else:
        cartaJugador = 'X'

    for i in range(1, 10):
        copiar = obtenerCopiaTablero(tablero)
        if esEspacioVacio(copiar, i):
            hacerMovimiento(copiar, cartaComputadora, i)
            if esGanador(copiar, cartaComputadora):
                return i

    for i in range(1, 10):
        copiar = obtenerCopiaTablero(tablero)
        if esEspacioVacio(copiar, i):
            hacerMovimiento(copiar, cartaJugador, i)
            if esGanador(copiar, cartaJugador):
                return i

    movimiento = elegirMovimientoAleatorioLista(tablero, [1, 3, 7, 9])
    if movimiento != None:
        return movimiento

    if esEspacioVacio(tablero, 5):
        return 5

    return elegirMovimientoAleatorioLista(tablero, [2, 4, 6, 8])

def esTableroLleno(tablero):
    for i in range(1, 10):
        if esEspacioVacio(tablero, i):
            return False
    return True


print('¡Bienvenido a 3NR!')

while True:
    elTablero = [' '] * 10
    cartaJugador, cartaComputadora = introducirCarta()
    turno = empiezaJugador()
    print(turno + ' irá primero.')
    jugandoJuego = True

    while jugandoJuego:
        if turno == 'jugador':
            dibujarTablero(elTablero)
            movimiento = obtenerMovimientoJugador(elTablero)
            hacerMovimiento(elTablero, cartaJugador, movimiento)

            if esGanador(elTablero, cartaJugador):
                dibujarTablero(elTablero)
                print('¡Ganaste el juego!')
                jugandoJuego = False
            else:
                if esTableroLleno(elTablero):
                    dibujarTablero(elTablero)
                    print('¡El juego es un empate!')
                    break
                else:
                    turno = 'computadora'

        else:
            movimiento = obtenerMovimientoComputadora(elTablero, cartaComputadora)
            hacerMovimiento(elTablero, cartaComputadora, movimiento)

            if esGanador(elTablero, cartaComputadora):
                dibujarTablero(elTablero)
                print('¡La computadora te ha ganado! Perdiste.')
                jugandoJuego = False
            else:
                if esTableroLleno(elTablero):
                    dibujarTablero(elTablero)
                    print('¡Empate en el juego!')
                    break
                else:
                    turno = 'jugador'

    if not jugarDeNuevo():
        break
